const anpro = require('anyproxy');

const option = {
    port: 8001,
    rule: require('./rule/gueig'),
    webInterface: {
        enable: true,
        webProt: 8002
    },
    throttle: 10000,
    forceProxyHttps: true,
    wsIntercept: false,
    silent: false

}
const pandpro = new anpro.ProxyServer(option);

pandpro.on('ready',() => {
    console.log("开始了")
})

pandpro.on('error',()=>{
    console.log("错误了")
})

pandpro.start()
